// 1. Animal Class
// Release 0
console.log('1. Animal Class ');
class Animal {
    constructor(name) {
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }
}

console.log('Release 0');
const sheep = new Animal("shaun");

console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)

// Release 1
console.log('Release 1');
class Frog extends Animal {
    constructor(name) {
        super(name);
        this.name = name;
    }
    jump() {
        console.log("hop hop");
    }
}

class Ape extends Animal {
    constructor(name) {
        super(name);
        this.name = name;
    }
    yell() {
        console.log("Auooo");
    }
}

const sungokong = new Ape("kera sakti")
sungokong.yell()

const kodok = new Frog("buduk")
kodok.jump()

console.log('-------------------------------');

// 2. Function to Class
console.log('2. Function to Class');

class Clock {
    constructor({ template }) {
        this.template = template
        this.timer;
    }

    render() {
        const date = new Date();

        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        const output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs)

        console.log(output);
    }

    stop() {
        clearInterval(this.timer);
    };

    start() {
        this.render();
        this.timer = setInterval(() => this.render(), 1000);
    };

}

const clock = new Clock({ template: 'h:m:s' });
clock.start();

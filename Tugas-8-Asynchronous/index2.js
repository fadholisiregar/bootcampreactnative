const readBooksPromise = require('./promise');

const books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]


readBooksPromise(10000, books[0])
    .then(sisaWaktu => {
        readBooksPromise(sisaWaktu, books[1])
            .then(sisaWaktu => {
                readBooksPromise(sisaWaktu, books[2])
                    .then(sisaWaktu => {
                        readBooksPromise(sisaWaktu, books[0])
                            .then(sisaWaktu => {
                                return sisaWaktu
                            })
                    })
            })
    })
    .catch(rejectedReason => {
        console.log(rejectedReason)
    })


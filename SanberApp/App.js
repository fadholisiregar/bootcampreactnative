import { StatusBar } from 'expo-status-bar';
import React from 'react';

import Tugas12 from './Tugas/Tugas12/App';
import Tugas13 from './Tugas/Tugas13/App';
import Tugas14 from './Tugas/Tugas14/App';
import Tugas15 from './Tugas/Tugas15/index';
// import Quis3 from './Quiz3/index';
import FinalProject from './finalProject/index';


export default function App() {
  return (
    // <Tugas12 />
    // <Tugas13 />
    // <Tugas14 />
    // <Tugas15 />
    // <Quis3 />
    <FinalProject />
  );
}


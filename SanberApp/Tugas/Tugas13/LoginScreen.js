import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TextInput } from 'react-native';

export default class Login extends Component {
    constructor() {
        super()
        this.state = {
            username: "",
            password: "",
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.logo}>
                    <Image source={require('./images/hiring.png')} style={{ width: 100, height: 100 }} />
                    <Text style={styles.logoText}>DevHireApp</Text>
                </View>
                <View style={styles.loginSentence}>
                    <Text style={{ fontWeight: 'bold' }}>Login and Start to Hire</Text>
                    <Text style={{ fontWeight: 'bold' }}>Profesional Developer</Text>
                </View>
                <View>
                    <View>
                        <TextInput
                            type="text"
                            placeholder="Username"
                            style={styles.input}
                            value={this.state.username}
                            onChangeText={(username) => this.setState({ username })} />
                        <TextInput
                            secureTextEntry={true}
                            placeholder="Password"
                            style={styles.input}
                            value={this.state.password}
                            onChangeText={(password) => this.setState({ password })}
                        />
                        <Text style={styles.forgetPassword}>forget password?</Text>
                    </View>
                    <View style={styles.tombol}>
                        <Text style={styles.tombolText}>Login</Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        margin: 32
    },
    logo: {
        alignItems: 'center',
    },
    logoText: {
        color: '#02157B',
        fontSize: 20,
        fontWeight: 'bold',
        margin: 'auto',
        textAlign: 'center',
        marginBottom: 27
    },
    loginSentence: {
        textAlign: 'left',
        marginBottom: 20,
    },
    forgetPassword: {
        color: '#02157B',
        textAlign: 'right',
        marginBottom: 20
    },
    input: {
        width: 300,
        borderWidth: 1,
        borderRadius: 10,
        padding: 8,
        marginVertical: 10,
        backgroundColor: '#fff',
        borderColor: 'gray'
    },
    tombol: {
        width: 300,
        borderWidth: 1,
        borderRadius: 10,
        paddingVertical: 11,
        marginVertical: 5,
        backgroundColor: '#02157B',
        borderColor: '#02157B'
    },
    tombolText: {
        textAlign: 'center',
        color: '#fff',
        fontWeight: 'bold'
    }
})



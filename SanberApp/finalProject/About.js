import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class Login extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.navBar}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                        <Image source={require('./images/arrow_back.png')} style={{ color: 'white' }} />
                    </TouchableOpacity>
                    <Text style={styles.textNavbar}>ABOUT APPS</Text>
                </View>

                <View style={styles.body}>
                    <View>
                        <Image source={require('./images/statistics.png')} style={{ width: 200, height: 200 }} />
                    </View>
                    <Text style={styles.aboutApp}>
                        Aplikasi KabarCorona memberikan informasi berupa data positif, sembuh, dan meninggal akibat terkena virus covid-19 yang ada di Indonesia. Pengguna dapat langsung melihat detail kasus pada setiao provinsi. Data akan diperbaharui setiap harinya untuk mempercepat informasi yang masuk dan dapat dengan cepat dilihat pengguna
                    </Text>
                </View>
                <View style={styles.tabBar}>
                    <TouchableOpacity
                        style={styles.tabItem}
                        onPress={() => this.props.navigation.navigate('Home')}>
                        <Icon name='home' size={25} />
                        <Text style={styles.tabTitle}>Home</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name='article' size={25} />
                        <Text style={styles.tabTitle}>News</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.tabItem}
                        onPress={() => this.props.navigation.navigate('About')}>
                        <Icon name='phone-android' size={25} />
                        <Text style={styles.tabTitle}>About Apps</Text>
                    </TouchableOpacity>
                </View>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    navBar: {
        height: 84,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        backgroundColor: '#EE6161',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
    },
    textNavbar: {
        color: 'white',
        marginLeft: 60,
        fontWeight: 'bold',
        fontSize: 24,
    },
    body: {
        flex: 1,
        marginTop: 20,
        alignItems: 'center'
    },
    aboutApp: {
        fontSize: 16,
        margin: 20,
        padding: 24,
        backgroundColor: '#EE6161',
        color: '#fff',
        borderRadius: 15,
    },
    tabBar: {
        backgroundColor: 'white',
        height: 60,
        borderTopWidth: 0.5,
        borderColor: '#E5E5E5',
        flexDirection: 'row',
        justifyContent: 'space-around'
    }, tabItem: {
        alignItems: 'center',
        justifyContent: 'center'
    }, tabTitle: {
        fontSize: 11,
        color: '#3c3c3c',
        paddingTop: 4
    }
})



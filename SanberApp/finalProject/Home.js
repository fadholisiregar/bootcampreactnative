import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ScrollView, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class Login extends Component {
  state = {
    dataID: [],
    dataProvinsi: []
  }

  getIndonesiaData() {
    fetch('https://api.kawalcorona.com/indonesia')
      .then(res => res.json())
      .then(res => {
        this.setState({ dataID: res[0] })
      })
  }
  getProvinceData() {
    fetch('https://api.kawalcorona.com/indonesia/provinsi')
      .then(res => res.json())
      .then(res => {
        this.setState({ dataProvinsi: res })
      })
  }

  componentDidMount() {
    this.getIndonesiaData();
    this.getProvinceData();
  }

  render() {
    const { navigate } = this.props.navigation;
    const { dataID } = this.state
    const provinsi = this.state.dataProvinsi.slice(0, 10).map(data => {
      return <TouchableOpacity
        key={data.attributes.Kode_Provi}
        style={styles.dataCovid}
        onPress={() => navigate('Detail',
          { provinsi: data.attributes })}
      >
        <Text
          style={{ fontWeight: 'bold', fontSize: 16 }}
        >{data.attributes.Provinsi}
        </Text>
      </TouchableOpacity>
    })
    return (
      <View style={styles.container}>
        <ScrollView style={styles.body}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={{ fontWeight: 'bold', marginTop: 25, fontSize: 18 }}>Hallo, {this.props.route.params.username}</Text>
            <TouchableOpacity>
              <Text
                style={{ fontWeight: 'bold', marginTop: 25, fontSize: 18 }}
                onPress={() => navigate('Login')}
              >Signout</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.jumbotron}>
            <Image source={require('./images/woman.png')}
              style={{ width: 100, height: 100, marginTop: 20 }}
            />
            <View style={{ width: 190, height: 60, marginTop: 20, marginBottom: 30 }}>
              <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 18, marginBottom: 5 }}>JANGAN LUPA 3M</Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>Memakai Masker</Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>Mencuci Tangan</Text>
              <Text style={{ color: '#fff', fontWeight: 'bold' }}>Menjaga Jarak</Text>
            </View>
          </View>
          <View style={{ marginTop: 20 }}>
            <Text style={{ color: 'black', fontWeight: 'bold', fontSize: 20 }}>Indonesia</Text>
            <View style={styles.dataContainer}>
              <View style={styles.dataCovid}>
                <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#FF0202' }}>Positif</Text>
                <Text style={{ marginTop: 10, fontWeight: 'bold', fontSize: 18, color: '#FF0202' }}>{dataID.positif}</Text>
              </View>
              <View style={styles.dataCovid}>
                <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#227AFF' }}>Dirawat</Text>
                <Text style={{ marginTop: 10, fontWeight: 'bold', fontSize: 18, color: '#227AFF' }}>{dataID.dirawat}</Text>
              </View>
              <View style={styles.dataCovid}>
                <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#00D789' }}>Sembuh</Text>
                <Text style={{ marginTop: 10, fontWeight: 'bold', fontSize: 18, color: '#00D789' }}>{dataID.sembuh}</Text>
              </View>
              <View style={styles.dataCovid}>
                <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#797979' }}>Meninggal</Text>
                <Text style={{ marginTop: 10, fontWeight: 'bold', fontSize: 18, color: '#797979' }}>{dataID.meninggal}</Text>
              </View>
            </View>
          </View>
          <View style={{ marginTop: 5 }}>
            <Text style={{ color: 'black', fontWeight: 'bold', fontSize: 20 }}>Provinsi</Text>
            <View style={styles.dataContainer}>
              {provinsi}
            </View>
          </View>
        </ScrollView>
        <View style={styles.tabBar}>
          <TouchableOpacity
            style={styles.tabItem}
            onPress={() => navigate('Home')}>
            <Icon name='home' size={25} />
            <Text style={styles.tabTitle}>Home</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
            <Icon name='article' size={25} />
            <Text style={styles.tabTitle}>News</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.tabItem}
            onPress={() => navigate('About')}>
            <Icon name='phone-android' size={25} />
            <Text style={styles.tabTitle}>About Apps</Text>
          </TouchableOpacity>
        </View>
      </View>



    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  body: {
    margin: 10,
  },
  jumbotron: {
    backgroundColor: '#F67777',
    height: 120,
    borderRadius: 15,
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  dataContainer: {
    marginTop: 15,
    borderColor: '#E5E5E5',
    justifyContent: 'space-around',
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap'
  },
  dataCovid: {
    width: 140,
    height: 90,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: '#E5E5E5',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    elevation: 3,
    marginBottom: 20,
  },
  tabBar: {
    backgroundColor: 'white',
    height: 60,
    borderTopWidth: 0.5,
    borderColor: '#E5E5E5',
    flexDirection: 'row',
    justifyContent: 'space-around'
  }, tabItem: {
    alignItems: 'center',
    justifyContent: 'center'
  }, tabTitle: {
    fontSize: 11,
    color: '#3c3c3c',
    paddingTop: 4
  }
})



import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';

export default class Login extends Component {
    render() {
        const provinsi = this.props.route.params.provinsi
        return (
            <View style={styles.container}>
                <View style={styles.navBar}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.goBack()}
                    >
                        <Image
                            source={require('./images/arrow_back.png')}
                            style={{ color: 'white' }}
                        />
                    </TouchableOpacity>
                    <Text style={styles.textNavbar}>DETAIL</Text>
                </View>

                <View style={styles.body}>
                    <Text style={styles.provinceName}>{provinsi.Provinsi}</Text>
                    <View style={styles.data}>
                        <Text style={{ fontWeight: 'bold', fontSize: 30, color: '#FF0202' }}>Positif</Text>
                        <Text style={{ fontWeight: 'bold', fontSize: 30, color: '#FF0202' }}>{provinsi.Kasus_Posi}</Text>
                    </View>
                    <View style={styles.data}>
                        <Text style={{ fontWeight: 'bold', fontSize: 30, color: '#00D789' }}>Sembuh</Text>
                        <Text style={{ fontWeight: 'bold', fontSize: 30, color: '#00D789' }}>{provinsi.Kasus_Semb}</Text>
                    </View>
                    <View style={styles.data}>
                        <Text style={{ fontWeight: 'bold', fontSize: 30, color: '#797979' }}>Meninggal</Text>
                        <Text style={{ fontWeight: 'bold', fontSize: 30, color: '#797979' }}>{provinsi.Kasus_Meni}</Text>
                    </View>
                </View>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    navBar: {
        height: 84,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        backgroundColor: '#EE6161',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
    },
    textNavbar: {
        color: 'white',
        marginLeft: 90,
        fontWeight: 'bold',
        fontSize: 24,
        textAlign: 'center',
    },
    body: {
        flex: 1,
    },
    data: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginBottom: 20
    },
    provinceName: {
        fontWeight: 'bold',
        fontSize: 40,
        marginBottom: 50,
        marginTop: 100,
        textAlign: 'center'
    }
})



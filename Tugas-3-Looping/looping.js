// No. 1 Looping While
var firstLoop = 1;
console.log('LOOPING PERTAMA');
while (firstLoop < 20) {
    console.log(firstLoop + 1 + " - I love coding");
    firstLoop += 2
}

var secondLoop = 21;
console.log('LOOPING KEDUA');
while (secondLoop > 1) {
    console.log(secondLoop - 1 + " - I will become a mobile developer");
    secondLoop -= 2
}

console.log('-------------------------------')

// No. 2 Looping menggunakan for
for (var angka = 1; angka <= 20; angka++) {
    if (angka % 2 !== 0 && angka % 3 === 0) {
        console.log(angka + " - I Love Coding");
    }
    else if (angka % 2 === 0) {
        console.log(angka + " - Berkualitas");
    }
    else if (angka % 2 !== 0) {
        console.log(angka + " - Santai");
    }
}

console.log('-------------------------------')

// No. 3 Membuat Persegi Panjang #
var column = 8
var row = 4

for (var i = 0; i < row; i++) {
    var str = ""
    for (var j = 1; j <= column; j++) {
        str += "#"
    }
    console.log(str);
}

console.log('-------------------------------')

// No. 4 Membuat Tangga
var angka = 7
for (var i = 0; i < angka; i++) {
    var str = ""
    for (var j = 0; j < (i + 1); j++) {
        str += "#"
    }
    console.log(str)
}

console.log('-------------------------------')

// No. 5 Membuat Papan Catur
var size = 8
var str

for (var i = 1; i <= size; i++) {
    if (i % 2 === 0) {
        str = "#"
        for (var j = 1; j < size; j++) {
            if (j % 2 === 0) {
                str += "#"
            } else if (j % 2 !== 0) {
                str += " "
            }
        }
    } else if (i % 2 !== 0) {
        str = " "
        for (var j = 1; j < size; j++) {
            if (j % 2 === 0) {
                str += " "
            } else if (j % 2 !== 0) {
                str += "#"
            }
        }
    }

    console.log(str);
}
// Soal No. 1 (Array to Object)
console.log("Soal No. 1 (Array to Object)");

function arrayToObject(arr) {
    var nowDate = new Date()
    var nowYear = nowDate.getFullYear()
    var age;

    for (var i = 0; i < arr.length; i++) {
        if (!arr[i][3] || arr[i][3] > nowYear) {
            age = "Invalid Birth Year";
        } else {
            age = nowYear - arr[i][3];
        }
        var obj = {
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age: age,
        }

        console.log(i + 1 + ". " + obj.firstName + obj.lastName + ": ");
        console.log(obj);
    }
}

var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]];
var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]];

arrayToObject(people);
arrayToObject(people2);
arrayToObject([]);

console.log('-------------------------------');

// Soal No. 2 (Shopping Time)
console.log('Soal No. 2 (Shopping Time)');

function shoppingTime(memberId, money) {
    var items = [
        { name: 'Sepatu brand Stacattu', price: 1500000 },
        { name: 'Baju brand Zoro', price: 500000 },
        { name: 'Baju brand H&N', price: 250000 },
        { name: 'Sweater brand Uniklooh', price: 175000 },
        { name: 'Casing Handphone', price: 50000 },
    ]

    var shoppingObj = {
        memberId: memberId,
        money: money,
        listPurchased: [],
        changeMoney: 0
    }
    if (memberId == '' || memberId == null) {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja';
    } else if (money < 50000) {
        return "Mohon maaf, uang tidak cukup"
    }

    for (var i = 0; i < items.length; i++) {
        if (money >= items[i].price) {
            shoppingObj['listPurchased'].push(items[i].name)
            money = money - items[i].price
        }
    }
    shoppingObj.changeMoney = money
    return shoppingObj;



}

console.log(shoppingTime('324193hDew2', 700000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());

console.log('-------------------------------');

// Soal No. 3 (Naik Angkot)
console.log('Soal No. 3 (Naik Angkot)');

function naikAngkot(listPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];

    var arrayPenumpang = []
    for (var i = 0; i < listPenumpang.length; i++) {
        var objPenumpang = {
            penumpang: listPenumpang[i][0],
            naikDari: listPenumpang[i][1],
            tujuan: listPenumpang[i][2],
            bayar: 0,
        }

        objPenumpang.bayar = 2000 * (rute.indexOf(objPenumpang.tujuan) - rute.indexOf(objPenumpang.naikDari));

        arrayPenumpang.push(objPenumpang)
    }

    return arrayPenumpang;
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log([]);

//  Soal No. 1 (Range)
console.log('Soal No. 1 (Range)');
function range(startNum, endNum) {
    var array = [];
    if (!endNum || (!startNum && !endNum)) {
        return -1
    } else if (startNum >= endNum) {
        for (var i = startNum; i >= endNum; i--) {
            array.push(i);
        }
    } else if (startNum <= endNum) {
        for (var i = startNum; i <= endNum; i++) {
            array.push(i);
        }
    }

    return array;
}

console.log(range(1, 10));
console.log(range(1));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());

console.log('-------------------------------');

// Soal No. 2 (Range with Step)
console.log('Soal No. 2 (Range with Step)');
function rangeWithStep(startNum, endNum, step) {
    var array = [];
    if (startNum >= endNum) {
        for (var i = startNum; i >= endNum; i -= step) {
            array.push(i);
        }
    } else if (startNum <= endNum) {
        for (var i = startNum; i <= endNum; i += step) {
            array.push(i);
        }
    }

    return array;
}

console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

console.log('-------------------------------');

// Soal No. 3 (Sum of Range)
console.log('Soal No. 3 (Sum of Range)');

function sum(startNum, endNum, step) {
    if (!startNum && !endNum) {
        return 0
    }
    else if (!endNum) {
        return 1
    }

    var array = [];
    if (startNum >= endNum) {
        for (var i = startNum; i >= endNum; step ? i -= step : i--) {
            array.push(i);
        }
    } else if (startNum <= endNum) {
        for (var i = startNum; i <= endNum; step ? i += step : i++) {
            array.push(i);
        }
    }

    return array.reduce((total, num) => total + num)
}
console.log(sum(1, 10));
console.log(sum(5, 50, 2));
console.log(sum(15, 10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());

console.log('-------------------------------');

// Soal No. 4(Array Multidimensi)
console.log('Soal No. 4 (Array Multidimensi)');

var biodata = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling(data) {
    for (var i = 0; i < data.length; i++) {
        console.log('Nomor ID: ' + data[i][0]);
        console.log('Nama Lengkap: ' + data[i][1]);
        console.log('TTL: ' + data[i][2] + ' ' + data[i][3]);
        console.log('Hobi: ' + data[i][4] + '\n');
    }
}

dataHandling(biodata)

console.log('-------------------------------');
// Soal No. 5 (Balik Kata)
console.log('Soal No. 5(Balik Kata)');

function balikKata(string) {
    return string.split("").reverse().join("")
}

console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));

console.log('-------------------------------');

// Soal No. 6 (Metode Array)
console.log('Soal No. 6 (Metode Array)');

function dataHandling2(arr) {
    arr.splice(1, 1, "Roman Alamsyah Elsharawy");
    arr.splice(2, 1, "Provinsi Bandar Lampung");
    arr.splice(5, 0, "Pria", "SMA Internasional Metro");
    console.log(arr);

    var date = arr[3].split("/")
    switch (date[1]) {
        case "01": {
            console.log("Januari");
            break;
        }
        case "02": {
            console.log("Februari");
            break;
        }
        case "03": {
            console.log("Maret");
            break;
        }
        case "04": {
            console.log("April");
            break;
        }
        case "05": {
            console.log("Mei");
            break;
        }
        case "06": {
            console.log("Juni");
            break;
        }
        case "07": {
            console.log("Juli");
            break;
        }
        case "08": {
            console.log("Agustus");
            break;
        }
        case "09": {
            console.log("September");
            break;
        }
        case "10": {
            console.log("Oktober");
            break;
        }
        case "11": {
            console.log("November");
            break;
        }
        case "12": {
            console.log("Desember");
            break;
        }
    }

    console.log(date.sort(function (value1, value2) { return value2 - value1 }));

    console.log(arr[3].split("/").join("-"));

    console.log(arr[1].slice(0, 15));
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
